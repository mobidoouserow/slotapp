//
//  ApiClient.h
//  CasinoAgregator
//
//  Created by Pavel Wasilenko on 03.05.17.
//  Copyright © 2017 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiClient : NSObject

- (NSString *)sendRequestAndShouldContinue;

@end
