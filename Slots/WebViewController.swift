//
//  WebViewController.swift
//  VulkanApp
//
//  Created by Alexey on 11/14/16.
//  Copyright © 2016 Alexey. All rights reserved.
//

import UIKit
//import CoreTelephony

class WebViewController: UIViewController {
    
    var webView: UIWebView!
    
    var url: String = ""
    var gameType: Int = 1
    
    let fileName = "index"
    let fileExt = "html"
    
//    let urlString = "http://rdtrck2.com/584ac2e23b209de3528b5d2c"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        let network_info = CTTelephonyNetworkInfo()
//        let carrier = network_info.subscriberCellularProvider?.isoCountryCode
        
//        let carrier = ""
        
//        let locale = Locale.current.regionCode
        
        
        var gameUrlString : String
        if gameType == 1 {
            gameUrlString = "v3-1"
        } else {
            gameUrlString = "v3-2"
        }
        
        
        let htmlFile = Bundle.main.path(forResource: fileName, ofType: fileExt, inDirectory: gameUrlString)
        
        
        var urlUrl = URL(fileURLWithPath: htmlFile!)
        
//        if carrier == "ru" || locale == "RU" {
//            urlUrl = (URL(string: urlString))!
//        }
        
        if self.url.characters.count > 0 {
            urlUrl = (URL(string: url))!
        }
        
        webView = UIWebView()
        view.addSubview(webView)
        webView.pinAllAtributes(toView: view, constant: 0)
        let request = URLRequest(url: urlUrl)
        webView.loadRequest(request)
        
    }
}
