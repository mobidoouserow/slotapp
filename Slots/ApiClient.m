//
//  ApiClient.m
//  CasinoAgregator
//
//  Created by Pavel Wasilenko on 03.05.17.
//  Copyright © 2017 Alexey. All rights reserved.
//

#import "ApiClient.h"
#import "QNSURLConnection.h"
#import <sys/utsname.h>

@implementation ApiClient

- (NSString *)sendRequestAndShouldContinue;
{
    //UNcomment to test
//    return @"";
    
    //TODO: 1) POST 2) send data
    
    /*
     Content-Type:application/json
     
     - package
     required
     string
     Пакет приложения
     com.example.app
     
     - locale
     string
     Локаль
     ru_RU
     
     - device_manufacturer
     string
     Производитель устройства
     LGE
     
     - device_model
     string
     Модель устройства
     Nexus 4
     */
    //result
    NSString *url = @"";
    
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    NSDictionary *postBodyDictionary =
    @{
      @"package" : [[NSBundle mainBundle] bundleIdentifier],
      @"locale" :  [[NSLocale currentLocale] localeIdentifier],
      @"device_manufacturer" : @"Apple",
      @"device_model" : code,
      };
    
    NSError * error;
    NSData * postBodyJsonData = [NSJSONSerialization dataWithJSONObject:postBodyDictionary options:0 error:&error];
    
    
    
    //initialize a request from url
    //http://appapi.me/api/check
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://appapi.me/api/check"]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:4];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postBodyJsonData];
    
    NSURLResponse *responce;
    NSError *requestError;
    
    NSData *hiddenData = [QNSURLConnection sendSynchronousRequest:request
                                                returningResponse:&responce
                                                            error:&requestError];
    
    if (hiddenData && !requestError) {
        //TODO: парсить
        NSString *body = [[NSString alloc] initWithData:hiddenData encoding:NSUTF8StringEncoding];
        
        NSLog(@"\n\n"
              "body : \n"
              "%@"
              "\n",
              body);
        
        NSError *jsonParseError;
        
        id object = [NSJSONSerialization JSONObjectWithData:hiddenData options:0 error:&jsonParseError];
        
        if (object && [object isKindOfClass:[NSDictionary class]]) {
            NSDictionary *resp = (NSDictionary *)object;
            
            if (resp &&
                resp[@"can_show"] &&
                resp[@"url"]
                )
            {
                NSNumber *respCanShow = resp[@"can_show"];
                NSString *respUrl = resp[@"url"];
                
                if ([respCanShow boolValue]) {
                    url = respUrl;
                }
            }
        }
    }
    
    
    return url;
}

@end
