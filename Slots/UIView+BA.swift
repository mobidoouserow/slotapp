//
//  UIView+BA.swift
//  Grasshoper
//
//  Created by Alexey on 9/25/16.
//  Copyright © 2016 AlexeyB. All rights reserved.
//

import UIKit

extension UIView {
    
    func pinAllAtributes(toView: UIView, constant: CGFloat) {
        let attributes = [
            NSLayoutAttribute.trailing,
            NSLayoutAttribute.leading,
            NSLayoutAttribute.top,
            NSLayoutAttribute.bottom
        ]
        pin(toView: toView, attributes: attributes, constant: constant)
    }
    
    func pin(toView: UIView, attributes: [NSLayoutAttribute], constant: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        for state in attributes {
            let constraint = NSLayoutConstraint(
                item: self,
                attribute: state,
                relatedBy: .equal,
                toItem: toView,
                attribute: state,
                multiplier: 1,
                constant: constant
            )
            toView.addConstraint(constraint)
        }
    }
    
}
