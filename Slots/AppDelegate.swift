//
//  AppDelegate.swift
//  SlotApp
//
//  Created by Alexey on 12/8/16.
//  Copyright © 2016 Alexey. All rights reserved.
//

import UIKit
import YandexMobileMetrica
import FBSDKCoreKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var webViewController: WebViewController?
    
    let yandexMetrickakey = "4e9dd348-467c-4897-8dd3-8be07cac1bc5"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        YMMYandexMetrica.activate(withApiKey: yandexMetrickakey)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let api = ApiClient()
        
        let url = api.sendRequestAndShouldContinue()
        
        if url == "" || url?.characters.count == 0 {
            let mainVC = GameSelectViewController(nibName: "GameSelectViewController", bundle: nil)
            window?.rootViewController = mainVC
        } else {
            self.webViewController = WebViewController()
            self.webViewController?.url = url!;
            self.webViewController?.gameType = 0
            self.window?.rootViewController = self.webViewController!
            self.window?.backgroundColor = UIColor.white
        }
        
        window?.makeKeyAndVisible()
        
        // Override point for customization after application launch.
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        return handled
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

//    func showWebViewWithUrl(urlString : String) {
//        /*
//         self.webviewController = [[UIViewController alloc] init];
//         
//         self.webviewController.view.frame = self.window.bounds;
//         self.webView = [[UIWebView alloc] initWithFrame:self.webviewController.view.bounds];
//         
//         if (![self.webviewController.view.subviews containsObject:self.webView])
//         {
//         [self.webviewController.view addSubview:self.webView];
//         }
//         
//         [self.window setRootViewController:self.webviewController];
//         [self.window makeKeyAndVisible];
//         
//         [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
//         */
//        
//        webViewController = UIViewController()
//        webViewController?.view.frame = (window?.bounds)!
//        
//        webView = UIWebView(frame: (webViewController?.view.bounds)!)
//        
//        webViewController?.view.addSubview(webView!)
//        
//        window?.rootViewController = webViewController
//        window?.makeKeyAndVisible()
//        
//        let url = URL(string: urlString)
//        
//        webView?.loadRequest(URLRequest(url: url!))
//    }
}

