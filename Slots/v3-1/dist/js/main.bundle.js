/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	__webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(2)();
	// imports


	// module
	exports.push([module.id, "* {\n  padding: 0;\n  margin: 0;\n  position: relative;\n  font-family: \"Arial\", sans-serif; }\n\nul {\n  list-style-type: none; }\n\nbr {\n  display: block;\n  margin: 0;\n  content: \" \"; }\n\n#fruit {\n  width: 568px;\n  height: 320px;\n  overflow: hidden;\n  background-color: #000;\n  background-image: url(" + __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"../img/background_main.jpg\""); e.code = 'MODULE_NOT_FOUND'; throw e; }())) + ");\n  background-size: 100%; }\n  #fruit .columns-wrap {\n    width: 454px;\n    height: 262px;\n    position: absolute;\n    top: 34px;\n    left: 58px;\n    overflow: hidden; }\n    #fruit .columns-wrap .column {\n      padding-top: 5px;\n      width: 77px;\n      float: left;\n      margin-right: 17px;\n      height: 100%; }\n      #fruit .columns-wrap .column:last-child {\n        margin-right: 0; }\n      #fruit .columns-wrap .column .card {\n        overflow: hidden;\n        height: 30%;\n        margin-bottom: 10px; }\n        #fruit .columns-wrap .column .card img {\n          width: 300%; }\n  #fruit .spin {\n    position: absolute;\n    right: 10px;\n    bottom: 50px;\n    width: 110px;\n    height: 110px;\n    overflow: hidden;\n    opacity: 0.8; }\n    #fruit .spin:hover {\n      opacity: 1; }\n    #fruit .spin img {\n      width: 300%; }\n  #fruit .numbers {\n    position: absolute;\n    width: 20px;\n    height: 90%;\n    top: 50px;\n    left: 10px;\n    overflow: hidden; }\n    #fruit .numbers img {\n      width: 200%;\n      left: -100%;\n      display: block; }\n    #fruit .numbers .empty {\n      height: 30px; }\n\n@media all and (width: 480px) {\n  #fruit {\n    width: 480px;\n    height: 320px; }\n    #fruit .columns-wrap {\n      width: 382px;\n      height: 217px;\n      top: 32px;\n      left: 49px; }\n      #fruit .columns-wrap .column {\n        width: 65px;\n        margin-right: 14px; }\n    #fruit .numbers {\n      width: 15px;\n      top: 45px; } }\n\n@media all and (width: 667px) {\n  #fruit {\n    width: 667px;\n    height: 375px; }\n    #fruit .columns-wrap {\n      width: 531px;\n      height: 308px;\n      top: 39px;\n      left: 68px; }\n      #fruit .columns-wrap .column {\n        width: 91px;\n        margin-right: 19px; }\n    #fruit .numbers {\n      width: 26px;\n      top: 45px; } }\n\n@media all and (width: 736px) {\n  #fruit {\n    width: 736px;\n    height: 414px; }\n    #fruit .columns-wrap {\n      width: 591px;\n      height: 340px;\n      top: 44px;\n      left: 75px; }\n      #fruit .columns-wrap .column {\n        width: 101px;\n        margin-right: 20px; }\n    #fruit .numbers {\n      width: 29px;\n      top: 45px; } }\n\n@media all and (width: 768px) {\n  body {\n    background-color: #09075a; }\n  #fruit {\n    width: 768px;\n    height: 490px;\n    top: 200px; }\n    #fruit .columns-wrap {\n      width: 614px;\n      height: 356px;\n      top: 46px;\n      left: 78px; }\n      #fruit .columns-wrap .column {\n        width: 106px;\n        margin-right: 20px; }\n    #fruit .numbers {\n      width: 32px; } }\n\n@media all and (width: 1024px) {\n  body {\n    background-color: #09075a; }\n  #fruit {\n    width: 1024px;\n    height: 648px;\n    top: 60px; }\n    #fruit .columns-wrap {\n      width: 824px;\n      height: 476px;\n      top: 59px;\n      left: 99px; }\n      #fruit .columns-wrap .column {\n        width: 142px;\n        margin-right: 27px; }\n    #fruit .numbers {\n      width: 32px; } }\n\n@media all and (width: 1024px) and (height: 1366px) {\n  body {\n    background-color: #09075a; }\n  #fruit {\n    width: 1024px;\n    height: 648px;\n    top: 350px; }\n    #fruit .columns-wrap {\n      width: 824px;\n      height: 476px;\n      top: 59px;\n      left: 99px; }\n      #fruit .columns-wrap .column {\n        width: 142px;\n        margin-right: 27px; }\n    #fruit .numbers {\n      width: 45px; } }\n\n@media all and (width: 1366px) {\n  body {\n    background-color: #09075a; }\n  #fruit {\n    width: 1366px;\n    height: 868px;\n    top: 60px; }\n    #fruit .columns-wrap {\n      width: 1084px;\n      height: 628px;\n      top: 82px;\n      left: 139px; }\n      #fruit .columns-wrap .column {\n        width: 187px;\n        margin-right: 37px; }\n    #fruit .numbers {\n      width: 66px; } }\n", ""]);

	// exports


/***/ },
/* 2 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];

		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};

		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }
/******/ ]);