var columns_wrap = document.querySelector('#fruit .columns-wrap');
var columns = [];
for (var i = 0; i < 5; i++) {
	var column = document.createElement('div');
	column.className = 'column';
	columns.push(column);
	columns_wrap.appendChild(column);
}

for (var i = columns.length - 1; i >= 0; i--) {
	var column = columns[i];
	for (var j = 0; j < 9; j++) {
		var card = document.createElement('div');
		card.className = 'card card-' + Math.round(Math.random() * 8);
		var img = document.createElement('img');
		img.src = 'dist/img/symbols.png';
		img.style.top = -1 * Math.round(Math.random() * 2) * 100 + '%';
		img.style.left = -1 * Math.round(Math.random() * 2) * 100 + '%';
		card.appendChild(img);
		column.appendChild(card);
	}	
}

var spin = document.querySelector('#fruit .spin');
spin.addEventListener('click', function() {
	for (var j = 0; j < 5; j++) {
		var rotate = function (column, number) {
			setTimeout(function(){
				for (var i = column.children.length - 1; i >= 0; i--) {
					column.children[i].style.top = -66.6 + '%';
				}
				column.insertBefore(column.children[column.children.length - 1], column.children[0]);
				column.insertBefore(column.children[column.children.length - 1], column.children[0]);
				var start = Date.now();

				var timer = setInterval(function() {
					var timePassed = Date.now() - start;
					if (timePassed >= 200) {
						clearInterval(timer);
						return;
					}

					for (var i = column.children.length - 1; i >= 0; i--) {
						column.children[i].style.top = parseFloat(column.children[i].style.top) - 33.3/4 + '%';
					}
				}, 25);
				if (number > 0) {
					number--;
					rotate(column, number);
				}
			}, 200);
		};
		rotate(columns[j], Math.round(Math.random() * 15) + 5);
	}

});